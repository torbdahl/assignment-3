# Assignment 3

This project is a Wep API for a simplified movie database made using Spring Web and PostgreSQL. The code is organized into a Controller-Service-Repository pattern and models movies, movie characters and movie franchises. The API offers basic functions to create, read, update and delete entries. 

## Entity models
The models for the entities are located in the package `no.noroff.accelerate.models` together with classes for the DTOs. 

### Franchise
`Franchise` has four columns:
- `franchise_id` - An auto-incremented integer which is used as primary key.
- `franchise_name` - A string designating the name of the franchise.
- `description` - A string with the description of the franchise.
- `movies` - A set of movies which belong to the franchise. This column has a OneToMany relationship with the Movie class where one franchise can have many movies. The Movie class has the foreign key, as required by relational design.

### Movie
`Movie` has nine columns:
- `movie_id` - An auto-incremented integer which is used as primary key.
- `title` - A string designating the title of the movie.
- `genre` - A string designating the genre of the movie.
- `release_year` - An integer indicating which year the movie was released.
- `director` - A string designating the director of the movie.
- `picture_url` - A string designating a url to a picture of the movie cover.
- `trailer_url` - A string designating a url to a trailer of the movie.
- `characters` - A set of characters belonging to the movie. This column has a ManyToMany mapping with the id column in MovieCharacter.
- `franchise_id` - An integer designating the id of the franchise which the movie belongs to. This column has a ManyToOne relationship with a franchise such that many movies can belong to one and the same franchise.

### MovieCharacter
`MovieCharacter` has six columns:
- `character_id` - An auto-incremented integer which is used as primary key.
- `character_name` - A string designating the name of the character.
- `alias` - A string designating the alias of the character.
- `gender` - A string designating the gender of the character.
- `picture_url` - A string designating a url to a picture of the movie cover.
- `movies` - A set of movies where the character appears. This column has a ManyToMany relationship to the Movie class.

## Controller-Service-Repository
The code is divided into three layers: The controller, service and repository layer. Each layer serve a separate purpose, making it easier to organize development, testing the functionality and maintaining the code.

### The controller layer
The purpose of this layer is to expose the functionality of the API such that it can be accessed by an external component. It lies at the very intersection between the backend and frontend of an application. 

It is implemented in package `no.noroff.accelerate.controllers` and consist of controller classes for each type of entity:
- `FranchiseController`
- `MovieCharacterController`
- `MovieController`

These controller classes use service and mapper classes, for example FranchiseService and FranchiseMapper, to access the service layer and to map Java objects into Data Transfer Objects. All the functions that return information about an entity do so using DTO representations. This is done for several reasons, but most importantly it limits the amount of information that is displayed to the consuming component and sent between the layers of the system.

Some functions are common to all controllers. These include:
- `getById(int id)` - Returns the entity having the provided id.
- `getAll()` - Returns all entities in the class.
- `add(DTO dto)` - Creates an entity from the provided DTO.
- `update(DTO dto, int id)` - Updates a given entity with the information contained in the DTO.
- `delete(int id)` - Deletes the entity having the provided id.

The `FranchiseController` contains two functions which are unique to its class:
- `getMoviesInFranchise(int id)` - Returns all the movies in the franchise having the provided id.
- `getMovieCharactersInFranchise(int id)` - Returns all the movie characters in the franchise having the provided id.

The `MovieController` also contains two functions unique to its class:
- `getCharactersInMovie(int id)` - Returns all the characters in a movie having the provided id.
- `updateCharactersInMovie(int movieId, int... characterIds)` - Replaces the set of characters in a given movie with the characters having the provided characterIds.

In addition, `FranchiseController` and `MovieController` have the function `checkIdExists(int id)` which returns a boolean to indicate whether the id is associated with an existing entity.

### The service layer
The purpose of this layer is to handle the business logic of the application. It determines how data can be created, stored and modified according to the goals of the business. This layer lies between the controller and repository layer.

It is implemented in the package `no.noroff.accelerate.services` and consist of an interface and an implementation class for each type of entity. Each service interface inherits from a generic CRUD interface called `CrudService` which has the following functions:
- `findById(ID id)` - Find an entity by id.
- `findAll()` - Fetch all entities.
- `add(T entity)` - Add new entity to table.
- `update(T entity)` - Update entity.
- `deleteById(ID id)` - Delete entity.

The `FranchiseService` interface has the following custom functions:
- `changeMoviesInFranchise(int franchiseId, int... movieIds)` - Replaces the movies in the franchise whose id matches the input franchiseId with the movies whose ids matches the input movieIds.
- `findMoviesInFranchise(int id)` - Fetches movies belonging to the franchise whose id matches the input id.
- `findMovieCharactersInFranchise(int id)` - Fetches movie characters belonging to the franchise whose id matches the input id
     
The `MovieService` interface has the following custom functions:
- `updateCharactersInMovie(int movieId, int... characterIds)` - Replaces the movie characters in the movie whose id matches the input movieId with the movie characters whose ids matches the input characterIds.
- `findCharactersInMovie(int id)` - Fetches the movie characters belonging to the movie whose id matches the input id.

Each `...ServiceImpl` class implements its corresponding interface and has access to a related `...Repository` class which further extends the `JpaRepository` class.

### The repository layer
The purpose of this layer is to enable storing and retrieval of data from a database. It is the bottom layer of our Controller-Service-Repository implementation pattern. 

The repository interfaces of this application are located in the package `no.noroff.accelerate.services`. The actual implementation of the repository interface is provided by the Spring framework. To avoid enlisting all these functions here, see [https://docs.spring.io/spring-data/jpa/docs/current/api/org/springframework/data/jpa/repository/JpaRepository.html](https://docs.spring.io/spring-data/jpa/docs/current/api/org/springframework/data/jpa/repository/JpaRepository.html)
