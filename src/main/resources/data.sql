insert into movie_character (alias, gender, character_name, picture_url)
values ('007', 'male', 'James Bond', 'http://www.jamesbond.com');

insert into movie_character (alias, gender, character_name, picture_url)
values ('006', 'male', 'Alec Trevelyan', 'http://www.alectrevelyan.com');

insert into movie_character (alias, gender, character_name, picture_url)
values ('Lee', 'male', 'Yan Naing Lee', 'http://www.rushhour.com');

insert into franchise (description, franchise_name)
values ('Rush Hour (Chinese: 火拼時速) is a 1998 American buddy action comedy film directed by Brett Ratner and written' ||
        ' by Jim Kouf and Ross LaManna from a story by LaManna. It stars Jackie Chan and Chris Tucker as mismatched' ||
        ' police officers who are assigned to rescue a Chinese diplomat''s abducted daughter. Tzi Ma, Tom Wilkinson,' ||
        ' Ken Leung, Mark Rolston, Elizabeth Peña, and Rex Linn play supporting roles. Released on September 18, 1998,' ||
        ' the film grossed over $244 million worldwide. The film''s box office commercial success led to two sequels:' ||
        ' Rush Hour 2 (2001) and Rush Hour 3 (2007).', 'Rush Hour');

insert into franchise (description, franchise_name)
values ('The James Bond series focuses on a fictional British Secret Service agent created in 1953 by writer Ian' ||
        ' Fleming, who featured him in twelve novels and two short-story collections. Since Fleming''s death in' ||
        ' 1964, eight other authors have written authorised Bond novels or novelisations: Kingsley Amis, Christopher ' ||
        'Wood, John Gardner, Raymond Benson, Sebastian Faulks, Jeffery Deaver, William Boyd, and Anthony Horowitz.' ||
        ' The latest novel is With a Mind to Kill by Anthony Horowitz, published in May 2022. Additionally Charlie' ||
        ' Higson wrote a series on a young James Bond, and Kate Westbrook wrote three novels based on the diaries of' ||
        ' a recurring series character, Moneypenny.', 'James Bond');

/*Movies*/

/* 007 movies. */
insert into movie (director, genre, picture_url, release_year, title, trailer_url, franchise_id)
values ('Michael G. Wilson Barbara Broccoli', 'action', 'https://upload.wikimedia.org/wikipedia/en/b/b9/Spectre_2015_poster.jpg',
        2015, 'Spectre', 'https://www.youtube.com/watch?v=ujmoYyEyDP8', 2);

insert into movie (director, genre, picture_url, release_year, title, trailer_url, franchise_id)
values ('Martin Campbell', 'action', 'https://upload.wikimedia.org/wikipedia/en/1/15/Casino_Royale_2_-_UK_cinema_poster.jpg',
        2006, 'Casino Royale', 'https://www.youtube.com/watch?v=36mnx8dBbGE', 2);

/* Rush Hour movies*/

insert into movie (director, genre, picture_url, release_year, title, trailer_url, franchise_id)
values ('Brett Ratner', 'action, comedy', 'https://m.media-amazon.com/images/M/MV5BYWM2NDZmYmYtNzlmZC00M2MyLWJmOGUtMjhiYmQ2OGU1YTE1L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_FMjpg_UX1000_.jpg',
        1998, 'Rush Hour', 'https://www.imdb.com/video/vi579274265/?playlistId=tt0120812&ref_=tt_ov_vi', 1);

insert into movie (director, genre, picture_url, release_year, title, trailer_url, franchise_id)
values ('Brett Ratner', 'action, comedy', 'https://m.media-amazon.com/images/M/MV5BODhlNGJjMWQtZGMyYS00MzJhLWJhZGMtY2NlNDI5Nzg5NTU2XkEyXkFqcGdeQXVyNDk3NzU2MTQ@._V1_.jpg',
        2001, 'Rush Hour 2', 'https://www.imdb.com/video/vi921043225/?playlistId=tt0266915&ref_=tt_pr_ov_vi', 1);

insert into movie_character_movies(characters_character_id, movies_movie_id)
values (1, 1);

insert into movie_character_movies(characters_character_id, movies_movie_id)
values (2, 2);

insert into movie_character_movies(characters_character_id, movies_movie_id)
values (3, 3);

insert into movie_character_movies(characters_character_id, movies_movie_id)
values (3, 4);
