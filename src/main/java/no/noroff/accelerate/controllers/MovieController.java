package no.noroff.accelerate.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.Response.ApiErrorResponse;
import no.noroff.accelerate.mappers.MovieCharacterMapper;
import no.noroff.accelerate.mappers.MovieMapper;
import no.noroff.accelerate.models.Franchise;
import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.models.MovieCharacter;
import no.noroff.accelerate.models.dtos.franchise.FranchiseDTO;
import no.noroff.accelerate.models.dtos.movie.MovieAddDTO;
import no.noroff.accelerate.models.dtos.movie.MovieDTO;
import no.noroff.accelerate.models.dtos.movie.MovieUpdateDTO;
import no.noroff.accelerate.models.dtos.moviecharacter.MovieCharacterDTO;
import no.noroff.accelerate.services.movie.MovieService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("rawtypes")
@RestController
@RequestMapping(path = "api/v1/movie") // Base URL.
public class MovieController {

    private final MovieService movieService;
    private final MovieMapper movieMapper;
    private final MovieCharacterMapper movieCharacterMapper;

    // Relevant service is injected.
    public MovieController(MovieService movieService, MovieMapper movieMapper, MovieCharacterMapper movieCharacterMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
        this.movieCharacterMapper = movieCharacterMapper;
    }


    //---------------------------------------------------------------
    @Operation(summary = "Gets a movie by their ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content =  {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})})

    @GetMapping("{id}") // GET: localhost:8080/api/v1/movie/1
    public ResponseEntity getById(@PathVariable int id) {
        MovieDTO movieDTO = movieMapper.movieToMovieDto(
                movieService.findById(id)
        );
        return ResponseEntity.ok(movieDTO);
    }

    //---------------------------------------------------------------
    @Operation(summary = "Get all movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))})})
    @GetMapping  // GET: localhost:8080/api/v1/movie
    public ResponseEntity getAll() {
        Collection<MovieDTO> movieDTOS = movieMapper.movieToMovieDto(
                movieService.findAll()
        );
        return ResponseEntity.ok(movieDTOS);
    }

    //---------------------------------------------------------------

    @Operation(summary = "Add a movie.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Bad request, check if all inputs are correct",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise Id does not exist",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })

    @PostMapping // POST: localhost:8080/api/v1/movie
    public ResponseEntity add(@RequestBody MovieAddDTO movieAddDTO) {
        Movie movie = movieService.add(movieMapper.movieAddDtoToMovie(movieAddDTO));
        URI location = URI.create("movie/" + movie.getId());
        return ResponseEntity.created(location).build();
    }

//---------------------------------------------------------------
    @Operation(summary = "update a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Supplied ID does not match",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "franchise does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})})

    @PutMapping("{id}") // PUT: localhost:8080/api/v1/movie/1
    public ResponseEntity update(@RequestBody MovieUpdateDTO movieUpdateDto, @PathVariable int id) {
        // Validates if body is correct.
        if (!checkIdExists(id) || id != movieUpdateDto.getId()) {
        }
        movieService.update(
                movieMapper.movieUpdateDtoToMovie(movieUpdateDto)
        );
        return ResponseEntity.noContent().build();
    }

    //---------------------------------------------------------------

    @Operation(summary = "delete a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "movie does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})})

    @DeleteMapping("{id}") // DELETE: localhost:8080/api/v1/movie/1
    public ResponseEntity delete(@PathVariable int id) {
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    //---------------------------------------------------------------

    @Operation(summary = "Get the characters in movie.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})})

    @GetMapping("{id}/characters")  // GET: localhost:8080/api/v1/franchise
    public ResponseEntity<Collection<MovieCharacterDTO>> getCharactersInMovie(@PathVariable int id) {
        ArrayList<MovieCharacterDTO> characterDtos = new ArrayList<>();
        for (MovieCharacter character : movieService.findCharactersInMovie(id)) {
            characterDtos.add(movieCharacterMapper.movieCharacterToMovieCharacterDto(character));
        }
        return ResponseEntity.ok(characterDtos);
    }

    /**
     * Helper function to check if id exists.
     * @param id
     * @return
     */
    private boolean checkIdExists(int id) {
        boolean idExists = false;
        for (Movie movie : movieService.findAll()) {
            if (movie.getId() == id) {
                idExists = true;
            }
        }
        return idExists;
    }

    //---------------------------------------------------------------
    @Operation(summary = "Update characters in a movie.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}),
            @ApiResponse(responseCode = "500",
                    description = "Character does not exist with supplied ID (No match)",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID (No match)",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})})

    @PutMapping("{movieId}/update-characters")  // GET: localhost:8080/api/v1/movie/1/update-characters
    public ResponseEntity updateCharactersInMovie(@PathVariable int movieId, @RequestBody int... characterIds) {
        if (!checkIdExists(movieId)) {
            return ResponseEntity.badRequest().build();
        }
        movieService.updateCharactersInMovie(movieId, characterIds);
        return ResponseEntity.noContent().build();
    }
}