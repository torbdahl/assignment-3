package no.noroff.accelerate.controllers;

import io.swagger.v3.oas.annotations.Operation;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.Response.ApiErrorResponse;
import no.noroff.accelerate.mappers.MovieCharacterMapper;
import no.noroff.accelerate.models.MovieCharacter;
import no.noroff.accelerate.models.dtos.moviecharacter.MovieCharacterDTO;
import no.noroff.accelerate.models.dtos.moviecharacter.MovieCharacterPostDTO;
import no.noroff.accelerate.models.dtos.moviecharacter.MovieCharacterUpdateDTO;
import no.noroff.accelerate.services.moviecharacter.MovieCharacterService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@SuppressWarnings("ALL")
@RestController
// First line for the url
@RequestMapping("api/v1/movieCharacter")
public class MovieCharacterController {

    //import of service and mapper for movieCharacter
    private final MovieCharacterService movieCharacterService;
    private final MovieCharacterMapper movieCharacterMapper;

    public MovieCharacterController(MovieCharacterService movieCharacterService, MovieCharacterMapper movieCharacterMapper) {
        this.movieCharacterService = movieCharacterService;
        this.movieCharacterMapper = movieCharacterMapper;
    }

    //---------------------------------------------------------------
    @Operation(summary = "Get all characters.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})})
    @GetMapping // GET:localhost:8080/api/v1/movieCharacter
    public ResponseEntity getAll(){
        Collection<MovieCharacterDTO> movieChar = movieCharacterMapper.movieCharacterToMovieCharacterDto(
                movieCharacterService.findAll());
        return ResponseEntity.ok(movieChar);
    }


    //---------------------------------------------------------------
    @Operation(summary = "Get character by id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacterDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("{id}") // GET: localhost:8080/api/v1/movieCharacter/1
    public ResponseEntity getById(@PathVariable int id){
        MovieCharacterDTO movieChar = movieCharacterMapper.movieCharacterToMovieCharacterDto(
                movieCharacterService.findById(id)
        );
        return ResponseEntity.ok(movieChar);
    }

    //---------------------------------------------------------------
    @Operation(summary = "Add a character.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacterDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PostMapping //POST: localhost:8080/api/v1/movieCharacters
    public ResponseEntity add(@RequestBody MovieCharacterPostDTO movieCharacterPostDTO){
        MovieCharacter movieCharacter = movieCharacterService.add(movieCharacterMapper.movieCharacterPostDtoToMovieCharacter(movieCharacterPostDTO));
        URI location = URI.create("movieCharacters/" + movieCharacter.getId());
        return ResponseEntity.created(location).build();
    }

    //---------------------------------------------------------------
    @Operation(summary = "Update a character.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacterDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PutMapping("{id}") // PUT: localhost:8080/api/v1/movieCharacter/1
    public ResponseEntity update(@RequestBody MovieCharacterUpdateDTO movieCharacterUpdateDTO, @PathVariable int id){
        boolean idExists = false;
        for (MovieCharacter movieCharacter : movieCharacterService.findAll()) {
            if (movieCharacter.getId() == id) {
                idExists = true;
            }
        }
        // Validate if body is correct and id exists.
        if (!idExists || id != movieCharacterUpdateDTO.getId())
            return ResponseEntity.badRequest().build();
        movieCharacterService.update(movieCharacterMapper.movieCharacterUpdateDtoToMovieCharacter(movieCharacterUpdateDTO));
    return ResponseEntity.noContent().build();
    }

    //---------------------------------------------------------------
    @Operation(summary = "Delete a character.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Deleted",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacterDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "No entity with id exists",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})})
    @DeleteMapping("{id}")
        public ResponseEntity delete(@PathVariable int id){
            movieCharacterService.deleteById(id);
            return ResponseEntity.noContent().build();
    }
}
