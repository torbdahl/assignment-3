package no.noroff.accelerate.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.Response.ApiErrorResponse;
import no.noroff.accelerate.mappers.FranchiseMapper;
import no.noroff.accelerate.mappers.MovieCharacterMapper;
import no.noroff.accelerate.mappers.MovieMapper;
import no.noroff.accelerate.models.Franchise;
import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.models.MovieCharacter;
import no.noroff.accelerate.models.dtos.franchise.FranchiseDTO;
import no.noroff.accelerate.models.dtos.franchise.FranchiseAddDTO;
import no.noroff.accelerate.models.dtos.franchise.FranchiseUpdateDTO;
import no.noroff.accelerate.models.dtos.movie.MovieDTO;
import no.noroff.accelerate.models.dtos.moviecharacter.MovieCharacterDTO;
import no.noroff.accelerate.services.franchise.FranchiseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/franchise") // Base URL.
public class FranchiseController {

    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;
    private final MovieMapper movieMapper;
    private final MovieCharacterMapper movieCharacterMapper;

    // Relevant service is injected.
    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper, MovieMapper movieMapper, MovieCharacterMapper movieCharacterMapper) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.movieMapper = movieMapper;
        this.movieCharacterMapper = movieCharacterMapper;
    }

    //---------------------------------------------------------------
    @Operation(summary = "Get franchise by id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})})

    @GetMapping("{id}") // GET: localhost:8080/api/v1/franchise/1
    public ResponseEntity getById(@PathVariable int id) {
        FranchiseDTO franchiseDTO = franchiseMapper.franchiseToFranchiseDto(
                franchiseService.findById(id)
        );
        return ResponseEntity.ok(franchiseDTO);
    }

    //---------------------------------------------------------------
    @Operation(summary = "Get all franchises.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))})})

    @GetMapping  // GET: localhost:8080/api/v1/franchise
    public ResponseEntity getAll() {
        Collection<FranchiseDTO> franchiseDTOS = franchiseMapper.franchiseToFranchiseDto(
                franchiseService.findAll()
        );
        return ResponseEntity.ok(franchiseDTOS);
    }

    //---------------------------------------------------------------
    @Operation(summary = "Add a franchise.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Bad request, check if inputs are correct",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })

    @PostMapping // POST: localhost:8080/api/v1/franchise
    public ResponseEntity add(@RequestBody FranchiseAddDTO franchiseAddDto) {
        Franchise franchise = franchiseService.add(franchiseMapper.franchiseAddDtoToFranchise(franchiseAddDto));
        URI location = URI.create("franchise/" + franchise.getId());
        return ResponseEntity.created(location).build();
    }

    //---------------------------------------------------------------
    @Operation(summary = "update franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Bad request, check if inputs are correct and matches input ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })

    @PutMapping("{id}") // PUT: localhost:8080/api/v1/franchise/1
    public ResponseEntity update(@RequestBody FranchiseUpdateDTO franchiseUpdateDto, @PathVariable int id) {
        // Validates if body is correct.
        if (!checkIdExists(id) || id != franchiseUpdateDto.getId()) {
            return ResponseEntity.badRequest().build();
        }
        franchiseService.update(
                franchiseMapper.franchiseUpdateDtoToFranchise(franchiseUpdateDto)
        );
        return ResponseEntity.noContent().build();
    }

    //---------------------------------------------------------------
    @Operation(summary = "delete a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Bad request, check if inputs are correct and matches input ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})})

    @DeleteMapping("{id}") // DELETE: localhost:8080/api/v1/franchise/1
    public ResponseEntity delete(@PathVariable int id){
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    //---------------------------------------------------------------
    @Operation(summary = "Update the movies in a franchise.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "franchise does not exist with supplied ID (No match)",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})})

    @PutMapping("{id}/update-movies") // PUT: localhost:8080/api/v1/franchise/1
    public ResponseEntity updateMoviesInFranchise(@PathVariable int id, @RequestBody int... movieIds) {
        if (!checkIdExists(id)) {
            return ResponseEntity.badRequest().build();
        }
        franchiseService.changeMoviesInFranchise(id, movieIds);
        return ResponseEntity.noContent().build();
    }

    //---------------------------------------------------------------
    @Operation(summary = "Get the movies in a franchise.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})})

    @GetMapping("{id}/movies")  // GET: localhost:8080/api/v1/franchise/1/movies
    public ResponseEntity<Collection<MovieDTO>> getMoviesInFranchise(@PathVariable int id) {
        ArrayList<MovieDTO> movieDtos = new ArrayList<>();
        for (Movie movie : franchiseService.findMoviesInFranchise(id)) {
            movieDtos.add(movieMapper.movieToMovieDto(movie));
        }
        return ResponseEntity.ok(movieDtos);
    }

    /**
     * Helper function to check if id exists.
     * @param id Entity id.
     * @return idExists Boolean indicating if id exists in table.
     */
    private boolean checkIdExists(int id) {
        boolean idExists = false;
        for (Franchise franchise : franchiseService.findAll()) {
            if (franchise.getId() == id) {
                idExists = true;
            }
        }
        return idExists;
    }

    //---------------------------------------------------------------
    @Operation(summary = "Get the characters in a franchise.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})})

    @GetMapping("{id}/movie-characters")  // GET: localhost:8080/api/v1/franchise/1/movie-characters
    public ResponseEntity<Collection<MovieCharacterDTO>> getMovieCharactersInFranchise(@PathVariable int id) {
        ArrayList<MovieCharacterDTO> movieCharacterDtos = new ArrayList<>();
        for (MovieCharacter movieCharacter : franchiseService.findMovieCharactersInFranchise(id)) {
            movieCharacterDtos.add(movieCharacterMapper.movieCharacterToMovieCharacterDto(movieCharacter));
        }
        return ResponseEntity.ok(movieCharacterDtos);
    }
}

