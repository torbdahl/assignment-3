package no.noroff.accelerate.services.movie;

import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.models.MovieCharacter;
import no.noroff.accelerate.services.CrudService;

import java.util.Collection;

public interface MovieService extends CrudService<Movie, Integer> {
    /**
     * Replaces the movie characters in the movie whose id matches the input movieId
     * with the movie characters whose ids matches the input characterIds
     * @param movieId Id of movie
     * @param characterIds Id of movie character
     * @return Updated movie
     */
    Movie updateCharactersInMovie(int movieId, int... characterIds);

    /**
     * Fetches the movie characters belonging to the movie whose id matches the input id
     * @param id Id of movie
     * @return List of movie characters
     */
    Collection<MovieCharacter> findCharactersInMovie(int id);
}
