package no.noroff.accelerate.services.movie;

import no.noroff.accelerate.exceptions.MovieCharacterNotFoundException;
import no.noroff.accelerate.exceptions.MovieNotFoundException;
import no.noroff.accelerate.models.Franchise;
import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.models.MovieCharacter;
import no.noroff.accelerate.repositories.MovieCharacterRepository;
import no.noroff.accelerate.repositories.MovieRepository;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
public class MovieServiceImpl implements MovieService {
    private final MovieRepository movieRepository;
    private final MovieCharacterRepository movieCharacterRepository;

    public MovieServiceImpl(MovieRepository movieRepository, MovieCharacterRepository movieCharacterRepository1) {
        this.movieRepository = movieRepository;
        this.movieCharacterRepository = movieCharacterRepository1;
    }

    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id)
                .orElseThrow(() -> new MovieNotFoundException(id));
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        // Null out movie from related franchises and characters.
        Movie movieToRemove = findById(id);
        movieToRemove.getFranchise().getMovies().remove(movieToRemove);
        movieToRemove.getCharacters().forEach(movieCharacter -> movieCharacter.getMovies().remove(movieToRemove));
        movieRepository.deleteById(id);
    }

    @Override
    public Movie updateCharactersInMovie(int movieId, int... characterIds) {
        Movie movie = movieRepository.findById(movieId).orElseThrow(() -> new MovieNotFoundException(movieId));
        // Remove movie from characters.
        for (MovieCharacter character : movie.getCharacters()) {
            Set<Movie> movies = character.getMovies();
            for (Movie m : movies) {
                if (m.getId() == movieId) {
                    movies.remove(m);
                }
            }
            // Update movies in character.
            character.setMovies(movies);
            // Save to database.
            movieCharacterRepository.save(character);
        }

        Set<MovieCharacter> characters = new HashSet<>();

        for (int characterId : characterIds) {
            MovieCharacter character = movieCharacterRepository.findById(characterId).orElseThrow(() -> new MovieCharacterNotFoundException(characterId));
            character.getMovies().add(findById(movieId));
            characters.add(character);
        }
        movie.setCharacters(characters);
        return movieRepository.save(movie);
    }

    @Override
    public Collection<MovieCharacter> findCharactersInMovie(int id) {
        Movie movie = movieRepository.findById(id).orElseThrow(() -> new MovieNotFoundException(id));
        return movie.getCharacters();
    }
}
