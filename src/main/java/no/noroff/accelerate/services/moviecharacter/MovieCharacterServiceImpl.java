package no.noroff.accelerate.services.moviecharacter;
import no.noroff.accelerate.models.MovieCharacter;
import no.noroff.accelerate.repositories.MovieCharacterRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class MovieCharacterServiceImpl implements MovieCharacterService {
    private final MovieCharacterRepository movieCharacterRepository;

    public MovieCharacterServiceImpl(MovieCharacterRepository movieCharacterRepository) {
        this.movieCharacterRepository = movieCharacterRepository;
    }

    @Override
    public MovieCharacter findById(Integer id) {
        return movieCharacterRepository.findById(id).orElseThrow(() -> new IllegalArgumentException());
    }

    @Override
    public Collection<MovieCharacter> findAll() {
        return movieCharacterRepository.findAll();
    }

    @Override
    public MovieCharacter add(MovieCharacter entity) {
        return movieCharacterRepository.save(entity);
    }

    @Override
    public MovieCharacter update(MovieCharacter entity) {
        return movieCharacterRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        movieCharacterRepository.deleteById(id);
    }
}
