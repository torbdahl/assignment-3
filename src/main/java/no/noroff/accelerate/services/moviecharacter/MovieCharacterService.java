package no.noroff.accelerate.services.moviecharacter;

import no.noroff.accelerate.models.MovieCharacter;
import no.noroff.accelerate.services.CrudService;

import java.util.Collection;

public interface MovieCharacterService extends CrudService<MovieCharacter, Integer> {
}
