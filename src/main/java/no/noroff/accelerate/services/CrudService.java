package no.noroff.accelerate.services;

import java.util.Collection;

public interface CrudService <T, ID> {
    /**
     * Find by Id
     * @param id
     * @return
     */
    T findById(ID id);

    /**
     * Fetch all
     * @return
     */
    Collection<T> findAll();

    /**
     * Add new to table
     * @param entity
     * @return
     */
    T add(T entity);

    /**
     * Update table
     * @param entity
     * @return
     */
    T update(T entity);

    /**
     * Delete based on id
     * @param id
     */
    void deleteById(ID id);
}
