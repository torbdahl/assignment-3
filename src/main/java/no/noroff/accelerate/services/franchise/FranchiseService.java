package no.noroff.accelerate.services.franchise;

import no.noroff.accelerate.models.Franchise;
import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.models.MovieCharacter;
import no.noroff.accelerate.services.CrudService;

import java.util.Collection;

public interface FranchiseService extends CrudService<Franchise, Integer> {
    /**
     * Replaces the movies in the franchise whose id matches the input franchiseId
     * with the movies whose ids matches the input movieIds
     * @param franchiseId Id franchise
     * @param movieIds Id movies
     * @return Updated franchise
     */
    Franchise changeMoviesInFranchise(int franchiseId, int... movieIds);

    /**
     * Fetches movies belonging to the franchise whose id matches the input id
     * @param id Id of franchise
     * @return List of movies
     */
    Collection<Movie> findMoviesInFranchise(int id);

    /**
     * Fetches movie characters belonging to the franchise whose id matches the input id
     * @param id Id of franchise
     * @return List of movie characters
     */
    Collection<MovieCharacter> findMovieCharactersInFranchise(int id);
}
