package no.noroff.accelerate.services.franchise;

import no.noroff.accelerate.exceptions.FranchiseNotFoundException;
import no.noroff.accelerate.exceptions.MovieNotFoundException;
import no.noroff.accelerate.models.Franchise;
import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.models.MovieCharacter;
import no.noroff.accelerate.repositories.FranchiseRepository;
import no.noroff.accelerate.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
public class FranchiseServiceImpl implements FranchiseService {
    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id)
                .orElseThrow(() -> new FranchiseNotFoundException(id));
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        // Null out franchise from all associated movies.
        findById(id).getMovies().forEach(
                movie -> {
                    if (movie.getFranchise().getId() == id) {
                        movie.setFranchise(null);
                    }
                });

        franchiseRepository.deleteById(id);
    }

    @Override
    public Franchise changeMoviesInFranchise(int franchiseId, int... movieIds) {
        Franchise franchise = franchiseRepository.findById(franchiseId).orElseThrow(() -> new FranchiseNotFoundException(franchiseId));
        Set<Movie> movies = new HashSet<>();
        // Set franchise column in movies from provided franchise to null such that only the provided movies are found in the given franchise.
        franchise.getMovies().forEach(
                movie -> {
                    movie.setFranchise(null);
                    movieRepository.save(movie);
        });

        for (int movieId : movieIds) {
            // Update franchise in movie.
            Movie movie = movieRepository.findById(movieId).orElseThrow(() -> new MovieNotFoundException(movieId));
            movie.setFranchise(franchise);
            // Put movie in franchise.
            movies.add(movie);
        }
        franchise.setMovies(movies);
        return franchiseRepository.save(franchise);
    }

    @Override
    public Collection<Movie> findMoviesInFranchise(int id) {
        Franchise franchise = franchiseRepository.findById(id).orElseThrow(() -> new FranchiseNotFoundException(id));
        return franchise.getMovies();
    }

    @Override
    public Collection<MovieCharacter> findMovieCharactersInFranchise(int id) {
        Franchise franchise = franchiseRepository.findById(id).orElseThrow(() -> new FranchiseNotFoundException(id));
        Set<Movie> movies = franchise.getMovies();
        Set<MovieCharacter> movieCharacters = new HashSet<>();
        for (Movie m : movies) {
            movieCharacters.addAll(m.getCharacters());
        }
        return movieCharacters;
    }
}
