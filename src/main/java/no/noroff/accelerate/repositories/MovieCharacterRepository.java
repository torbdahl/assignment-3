package no.noroff.accelerate.repositories;

import no.noroff.accelerate.models.MovieCharacter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieCharacterRepository extends JpaRepository<MovieCharacter, Integer> {
}
