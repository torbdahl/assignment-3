package no.noroff.accelerate.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class MovieCharacterNotFoundException extends RuntimeException {
    public MovieCharacterNotFoundException(int id) {
        super("Character does not exist with ID: " + id);
    }
    //in case of future usage
    public MovieCharacterNotFoundException(String message) {
        super(message);
    }
    public MovieCharacterNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
    public MovieCharacterNotFoundException(Throwable cause) {
        super(cause);
    }
    public MovieCharacterNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}