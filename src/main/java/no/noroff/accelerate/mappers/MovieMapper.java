package no.noroff.accelerate.mappers;

import no.noroff.accelerate.models.Franchise;
import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.models.MovieCharacter;
import no.noroff.accelerate.models.dtos.movie.MovieAddDTO;
import no.noroff.accelerate.models.dtos.movie.MovieDTO;
import no.noroff.accelerate.models.dtos.movie.MovieUpdateDTO;
import no.noroff.accelerate.services.franchise.FranchiseService;
import no.noroff.accelerate.services.moviecharacter.MovieCharacterService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    protected MovieCharacterService movieCharacterService;

    @Autowired
    protected FranchiseService franchiseService;

    @Mapping(target = "characters", source = "movie.characters", qualifiedByName = "mapCharactersToCharacterIds")
    @Mapping(target = "franchise", source = "movie.franchise", qualifiedByName = "mapFranchiseToId")
    public abstract MovieDTO movieToMovieDto(Movie movie);

    public abstract Collection<MovieDTO> movieToMovieDto(Collection<Movie> movies);

    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "mapIdToFranchise")
    @Mapping(target = "characters", ignore = true)
    public abstract Movie movieUpdateDtoToMovie(MovieUpdateDTO movieUpdateDto);

    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "mapIdToFranchise")
    @Mapping(target = "characters", ignore = true)
    @Mapping(target = "id", ignore = true)
    public abstract Movie movieAddDtoToMovie(MovieAddDTO movieAddDto);

    @Named("mapIdToFranchise")
    Franchise mapIdToFranchise(int id) {
        return franchiseService.findById(id);
    }

    @Named("mapFranchiseToId")
    Integer mapFranchiseToId(Franchise source) {
        if(source == null)
            return null;
        return source.getId();
    }

    @Named("mapCharactersToCharacterIds")
    Set<Integer> mapCharactersToCharacterIds(Set<MovieCharacter> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(MovieCharacter::getId).collect(Collectors.toSet());
    }
}
