package no.noroff.accelerate.mappers;

import no.noroff.accelerate.models.Franchise;
import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.models.dtos.franchise.FranchiseDTO;
import no.noroff.accelerate.models.dtos.franchise.FranchiseAddDTO;
import no.noroff.accelerate.models.dtos.franchise.FranchiseUpdateDTO;
import no.noroff.accelerate.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
    protected MovieService movieService;

    @Mapping(target = "movies", source = "franchise.movies", qualifiedByName = "mapMoviesToMovieIds")
    public abstract FranchiseDTO franchiseToFranchiseDto(Franchise franchise);

    public abstract Collection<FranchiseDTO> franchiseToFranchiseDto(Collection<Franchise> franchises);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "movies", ignore = true)
    public abstract Franchise franchiseAddDtoToFranchise(FranchiseAddDTO franchiseAddDTO);

    @Mapping(target = "movies", ignore = true)
    public abstract Franchise franchiseUpdateDtoToFranchise(FranchiseUpdateDTO franchiseUpdateDTO);

    @Named("mapMoviesToMovieIds")
    Set<Integer> mapMoviesToMovieIds(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(Movie::getId).collect(Collectors.toSet());
    }

    @Named("mapMovieIdsToMovies")
    Set<Movie> mapMovieIdsToMovies(Set<Integer> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(id -> movieService.findById(id)).collect(Collectors.toSet());
    }
}
