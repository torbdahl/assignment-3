package no.noroff.accelerate.mappers;

import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.models.MovieCharacter;
import no.noroff.accelerate.models.dtos.moviecharacter.MovieCharacterDTO;
import no.noroff.accelerate.models.dtos.moviecharacter.MovieCharacterPostDTO;
import no.noroff.accelerate.models.dtos.moviecharacter.MovieCharacterUpdateDTO;
import no.noroff.accelerate.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieCharacterMapper {
    @Autowired
    protected MovieService movieService;

    @Mapping(target = "movies", source = "movieCharacter.movies", qualifiedByName = "mapMoviesToMovieIds")
    public abstract MovieCharacterDTO movieCharacterToMovieCharacterDto(MovieCharacter movieCharacter);

    public abstract Collection<MovieCharacterDTO> movieCharacterToMovieCharacterDto(Collection<MovieCharacter> movieCharacters);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "movies", ignore = true)
    public abstract MovieCharacter movieCharacterPostDtoToMovieCharacter(MovieCharacterPostDTO movieCharacterPostDTO);

    @Mapping(target = "movies", ignore = true)
    public abstract MovieCharacter movieCharacterUpdateDtoToMovieCharacter(MovieCharacterUpdateDTO movieCharacterUpdateDTO);

    @Named("mapMovieIdsToMovies")
    Set<Movie> mapMoviesToMovieIds(Set<Integer> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(id -> movieService.findById(id)).collect(Collectors.toSet());
    }

    @Named("mapMoviesToMovieIds")
    Set<Integer> mapMovieIdsToMovies(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(Movie::getId).collect(Collectors.toSet());
    }
}
