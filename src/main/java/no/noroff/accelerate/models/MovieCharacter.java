package no.noroff.accelerate.models;

// One movie belongs to many characters, and a character can play in multiple movies.

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class MovieCharacter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "character_id")
    private int id;
    @Column (name = "character_name", length = 50, nullable = false)
    private String name;
    @Column (name = "alias", length = 50, nullable = false)
    private String alias;
    @Column (name = "gender",length = 50, nullable = false)
    private String gender;
    @Column (name = "picture_url", length = 50, nullable = false)
    private String picture_url;
    @ManyToMany
    private Set<Movie> movies;
}
