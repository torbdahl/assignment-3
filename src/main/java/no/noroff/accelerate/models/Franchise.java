package no.noroff.accelerate.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "franchise_id")
    private int id;
    @Column(name = "franchise_name", length = 200, nullable = false)
    private String name;
    @Column(name = "description", length = 1000, nullable = false)
    private String description;
    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;

    public Franchise(int id, String name, String description, Set<Movie> movies) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.movies = movies;
    }

    public Franchise() {
    }
}

