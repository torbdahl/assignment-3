package no.noroff.accelerate.models.dtos.movie;

import lombok.Data;

import java.util.Set;

@Data
public class MovieDTO {
    private int id;
    private String title;
    private String genre;
    private int release_year;
    private String director;
    private String picture_url;
    private String trailer_url;
    private Set<Integer> characters;
    private Integer franchise;
}
