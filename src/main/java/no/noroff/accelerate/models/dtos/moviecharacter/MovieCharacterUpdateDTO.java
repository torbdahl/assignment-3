package no.noroff.accelerate.models.dtos.moviecharacter;

import lombok.Data;

@Data
public class MovieCharacterUpdateDTO {
    private int id;
    private String name;
    private String alias;
    private String gender;
    private String picture_url;
}
