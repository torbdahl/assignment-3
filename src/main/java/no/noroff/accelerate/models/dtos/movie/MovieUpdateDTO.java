package no.noroff.accelerate.models.dtos.movie;

import lombok.Data;

@Data
public class MovieUpdateDTO {
    private int id;
    private String title;
    private String genre;
    private int release_year;
    private String director;
    private String picture_url;
    private String trailer_url;
    private Integer franchise;
}
