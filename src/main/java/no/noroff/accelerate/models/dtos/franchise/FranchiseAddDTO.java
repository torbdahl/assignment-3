package no.noroff.accelerate.models.dtos.franchise;

import lombok.Data;

@Data
public class FranchiseAddDTO {
    private String name;
    private String description;
}
