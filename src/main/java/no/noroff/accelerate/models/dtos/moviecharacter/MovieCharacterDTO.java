package no.noroff.accelerate.models.dtos.moviecharacter;

import lombok.Data;


import java.util.Set;

@Data
public class MovieCharacterDTO {
    private int id;
    private String name;
    private String alias;
    private String gender;
    private String picture_url;
    private Set<Integer> movies;
}

