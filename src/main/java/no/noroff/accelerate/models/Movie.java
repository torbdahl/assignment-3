package no.noroff.accelerate.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private int id;
    @Column(name = "title", length = 200, nullable = false)
    private String title;
    @Column(name = "genre", length = 200, nullable = false)
    private String genre;
    @Column(name = "release_year", length = 10, nullable = false)
    private int release_year;
    @Column(name = "director", length = 100, nullable = false)
    private String director;
    @Column(name = "picture_url", length = 200, nullable = false)
    private String picture_url;
    @Column(name = "trailer_url", length = 200, nullable = false)
    private String trailer_url;
    @ManyToMany(mappedBy = "movies")
    private Set<MovieCharacter> characters;
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;
}
